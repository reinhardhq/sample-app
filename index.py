# coding:utf-8

import rtsp

# debug
DEBUG = True

# rtsp server address
# Docker Container rtspatt
SERVER = "192.168.1.3:8554/live.sdp"

# rstp 1show
if DEBUG:
    client = rtsp.Client(rtsp_server_uri = 'rtsp://' + SERVER)
    client.read().show()
    client.close()

# rtsp stream
else:
    with rtsp.Client('rtsp://' + SERVER) as client:
        client.preview()


